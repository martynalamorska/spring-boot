package pl.sda.springbootex1.services;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import pl.sda.springbootex1.data.User;
import pl.sda.springbootex1.data.repository.UserRepository;
import pl.sda.springbootex1.exceptions.UserNotFoundException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Component
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void addUser(@RequestBody User user) {
        userRepository.save(user);
    }

    public List<User> findAllUsers() {
        return userRepository.findAll();
    }

    public void modifyUser(User user, long id) {
        userRepository.findById(id).map(
                userFromDatabase -> {
                    userFromDatabase.setFirstName(user.getFirstName());
                    userFromDatabase.setLastName(user.getLastName());
                    userFromDatabase.setGender(user.getGender());
                    userFromDatabase.setInterests(user.getInterests());
                    return userRepository.save(userFromDatabase);
                }
        ).orElseGet(() -> {
            user.setId(id);
            return userRepository.save(user);
        });
    }

    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }

    public User findById(Long id) {

//        krótka wersja:
//        return userRepository.findById(id).orElseThrow(UserNotFoundException::new);

        Optional<User> maybeUser = userRepository.findById(id);
        boolean present = maybeUser.isPresent();
        if (present) {
            return maybeUser.get();
        }
        throw new UserNotFoundException();
    }

    public List<User> findByFirstName(String firstName) {
        List<User> users = userRepository.findByFirstName(firstName);
        if (users.isEmpty()) {
            throw new UserNotFoundException("No users with matching first name found");
        } else {
            return users;
        }
    }

    public List<User> findByFirstNameAndLastName(String firstName, String lastName) {
        List<User> users = userRepository.findByFirstNameAndLastName(firstName, lastName);
        if (users.isEmpty()) {
            throw new UserNotFoundException("No users with matching first and last name found");
        } else {
            return users;
        }
    }

    public List<User> findByBirthdayBetween(String from, String to){
        return userRepository.findByBirthdayBetween(LocalDate.parse(from), LocalDate.parse(to));
    }


}
