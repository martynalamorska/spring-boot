package pl.sda.springbootex1.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.sda.springbootex1.data.User;

import java.time.LocalDate;
import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {

    List<User> findByFirstName(String firstName);

//    @Query("select u from User u where u.firstName =?1")
//    List<User> findByFirstNameQuery(String firstName);

    @Query("select u from User u where u.firstName = ?1 and u.lastName = ?2")
    List<User> findByFirstNameAndLastName(String firstName, String lastName);

    List<User> findByBirthdayBetween(LocalDate from, LocalDate to);

}
