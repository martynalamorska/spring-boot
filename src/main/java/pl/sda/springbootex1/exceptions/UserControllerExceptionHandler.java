package pl.sda.springbootex1.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDate;

@ControllerAdvice
public class UserControllerExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<UserErrorResponse> handleException(UserNotFoundException exception){

        UserErrorResponse userErrorResponse = UserErrorResponse.builder()
                .message(exception.getMessage())
                .status(HttpStatus.BAD_REQUEST.toString())
                .localDate(LocalDate.now())
                .build();
        return new ResponseEntity<>(userErrorResponse, HttpStatus.NOT_FOUND);

    }
}
