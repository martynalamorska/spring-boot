package pl.sda.springbootex1.configuration;


import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.sda.springbootex1.data.User;
import pl.sda.springbootex1.data.repository.UserRepository;

import java.time.LocalDate;

@Configuration
@Slf4j
public class LoadDataBase {

    @Bean
    public CommandLineRunner initDatabase(UserRepository userRepository){
        return args -> {
            log.info("Inserting to database " + userRepository.save(new User("Anna", "Nowak", "F", LocalDate.of(1990, 11, 11), "Sports")));
            log.info("Inserting to database " + userRepository.save(new User("Mike", "Mikovsky", "M", LocalDate.of(1980, 2, 25), "Medicine")));
            log.info("Inserting to database " + userRepository.save(new User("Jan", "Kowalski", "M", LocalDate.of(1965, 1, 31), "Cars")));
            log.info("Inserting to database " + userRepository.save(new User("Martyna", "Lamorska", "F", LocalDate.of(1996, 7, 3), "Food")));

        };
    }
}
