package pl.sda.springbootex1.controller;

import org.springframework.web.bind.annotation.*;

@RestController
public class SampleController {

    @GetMapping("/favourite/{number}")
//    to pod spodem to samo co GetMapping (zamiennik)
//    @RequestMapping(value = "/favourite/{number}", method = RequestMethod.GET)
    public String favNumber(@RequestParam(value = "name", defaultValue = "you") String name, @PathVariable String number) {
        if (number.matches("^[0-9]*$") && number.length() > 0) {
            return "Hello " + name + ", your favourite number is " + number;
        } else {
            return "Hello " + name + ", your favourite number is NaN";
        }
    }
}
