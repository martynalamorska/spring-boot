package pl.sda.springbootex1.controller;

import org.springframework.web.bind.annotation.*;
import pl.sda.springbootex1.data.User;
import pl.sda.springbootex1.services.UserService;

import java.time.LocalDate;
import java.util.List;

@RestController
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public void addUser(@RequestBody User user) {
        userService.addUser(user);
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public List<User> findAllUsers(){
        return userService.findAllUsers();
    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    public User findUserById(@PathVariable Long id){
        return userService.findById(id);
    }

    @RequestMapping(value = "/users/modify/{id}", method = RequestMethod.PUT)
    public void modifyUser(@RequestBody User user, @PathVariable Long id){
        userService.modifyUser(user, id);
    }

    @RequestMapping(value = "/users/delete/{id}", method = RequestMethod.DELETE)
    public void deleteUser(@PathVariable Long id){
        userService.deleteUser(id);
    }

    @RequestMapping(value = "/userByFirstName", method = RequestMethod.GET)
    public List<User> findByFirstName(@RequestParam(value = "firstName") String firstName){
        return userService.findByFirstName(firstName);
    }

    @RequestMapping(value = "/userByFirstLastName", method = RequestMethod.GET)
    public List<User> findByFirstNameAndLastName(@RequestParam(value = "firstName") String firstName, @RequestParam(value = "lastName") String lastName){
        return userService.findByFirstNameAndLastName(firstName, lastName);
    }

    @RequestMapping(value = "/userByBirthday", method = RequestMethod.GET)
    public List<User> findByBirthdayBetween(@RequestParam(value = "from") String from,
                                            @RequestParam(value = "to") String to){
        return userService.findByBirthdayBetween(from, to);
    }

}
